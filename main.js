Vue.component('tab-content', {
  template: '#tab-content',
  props: ['tab']
});

var r = new Vue({
  el: '#r',
  data: {
    tabs: [
      {
		debag: true,
        title: 'tab 1', 
        active: true,
		color: 'yellow',
        text: [
					
                    '1',
                    '2',
                    '3',
                ]
      },
      {
		debag: true,
        title: 'tab 2', 
        active: false,
		color: 'green',
        text: [
                    '4',
                    '5',
                    '6',
                ]
      },
      {
		debag: true,
        title: 'tab 3', 
        active: false,
		color: 'blue',
        text: [
                    '7',
                    '8',
                    '9',
                ]
      },
    ]
  },
  methods: {
    setActive(tab) {
      this.tabs.forEach(el => {
        el.active = el === tab;
      })
    }
  },
  computed: {
   debagTab: function () {
      return this.tabs.reduce((accum, curr) => {return curr.active ? curr : accum}, {});
    }
  }
});